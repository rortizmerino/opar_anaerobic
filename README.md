# Opar_anaerobic

Code for the manuscript entitled "Re-oxidation of cytosolic NADH is a major contributor to the high oxygen requirements of the thermotolerant yeast Ogataea parapolymorpha in oxygen-limited cultures"